# -*- coding: utf-8 -*-
"""
Created on Tue Jun  8 13:58:14 2021

@author: chase
"""

# FILTER VELOCITY AND FORCE PLOTS

# IMPORT LIBRARIES
import scipy.io as sio
from scipy import signal
import matplotlib.pylab as plt
import numpy as np

# UPLOAD .MAT FILE AND PRINT TO CHECK
data = sio.loadmat('MR_Data.mat')

# TIME DATA
time = data["time"]


# DISPLACEMENT DATA
sensor = data["X"]
displacement = sensor[:,1]


# FORCE DATA
force = sensor[:,2]

# VELOCITY DATA
velocity = np.zeros(time.shape[0])
for i in range(time.shape[0]):
    dx = displacement[i] - displacement[i-1]
    dt = time[i] - time[i-1]
    velocity[i] = dx/dt
    
# PLOT VELOCITY GRAPHS
fig, ax = plt.subplots(2,2)
fig.subplots_adjust(hspace=1, wspace=0.5)


# FILTER 1
n = 20000
velocity1 = signal.resample(velocity,n)
force1 = signal.resample(force, n)

ax[0,0].plot(velocity1, force1, 'tab:green')
ax[0,0].set_ylim(-5,5)
ax[0,0].set_xlim(-2,2)
ax[0,0].set_ylabel('Force (kip)')
ax[0,0].set_xlabel('Velocity (in/s)')
ax[0,0].set_title('Scipy Signal Resample')

# FILTER 2
ax[0,1].plot(signal.savgol_filter(velocity,11,6), signal.savgol_filter(force,199,6), 'tab:blue')
ax[0,1].set_ylim(-5,5)
ax[0,1].set_xlim(-2,2)
ax[0,1].set_ylabel('Force (kip)')
ax[0,1].set_xlabel('Velocity (in/s)')
ax[0,1].set_title('Savgol Filter')

# FILTER 3
def smooth(y, box_pts):
    box = np.ones(box_pts)/box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth

ax[1,0].plot(smooth(velocity,5000),smooth(force,5000), 'tab:red')
ax[1,0].set_ylim(-5,5)
ax[1,0].set_xlim(-2 ,2)
ax[1,0].set_ylabel('Force (kip)')
ax[1,0].set_xlabel('Velocity (in/s)')
ax[1,0].set_title('Moving Box Average')

# FILTER 4
b, a = signal.butter(3,0.1)

ax[1,1].plot(signal.filtfilt(b, a, velocity),signal.filtfilt(b, a, force), 'tab:purple')
ax[1,1].set_ylim(-5,5)
ax[1,1].set_xlim(-2 ,2)
ax[1,1].set_ylabel('Force (kip)')
ax[1,1].set_xlabel('Velocity (in/s)')
ax[1,1].set_title('FiltFilt Filter')
